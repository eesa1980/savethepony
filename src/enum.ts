export enum Direction {
  EAST = 'east',
  WEST = 'west',
  NORTH = 'north',
  SOUTH = 'south',
  STAY = 'stay'
}

export enum StateResult {
  MOVE_ACCEPTED = 'Move accepted',
  CANT_WALK_IN_THERE = "Can't walk in there"
}

export enum ColorPalette {
  BLACK = '#2C2C2C',
  BLUE = '#128AB2',
  TEAL = '#08D6A0',
  YELLOW = '#FFD166',
  PINK = '#EF476F'
}
