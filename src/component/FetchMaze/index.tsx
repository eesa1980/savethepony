import React, { ReactElement, useEffect, useState } from 'react'
import { ApiCalls } from '../../api/'
import { GetMazeDataResponse } from '../../types'

type PropTypes = {
  mazeId?: string
  children: (props: GetMazeDataResponse) => ReactElement
  showSpinner?: boolean
}

const apiCalls = ApiCalls()

export const FetchMaze = ({
  mazeId,
  children,
  showSpinner
}: PropTypes): ReactElement => {
  const [mazeData, set] = useState<GetMazeDataResponse | null>(null)

  useEffect(() => {
    if (!mazeId) return

    apiCalls.getMazeData(mazeId).then(set).catch(console.error)
  }, [])

  return mazeData ? (
    children(mazeData)
  ) : (
    <>{showSpinner && 'Fetching Maze...'}</>
  )
}
