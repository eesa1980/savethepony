import React, { ReactElement, useCallback, useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { GameState, GetMazeDataResponse } from '../../types'
import { ApiCalls } from '../../api'
import { Direction } from '../../enum'
import { PonyNavigator } from '../../util/PonyNavigator'
import { MazeSquare, Content, MazeGrid } from './index.styled'
import { MazeArrow } from '../MazeArrow'
import { Flag, Gremlin } from 'grommet-icons'

type PropTypes = {
  mazeData: GetMazeDataResponse
}

const apiCalls = ApiCalls()

export const Maze = (props: PropTypes): ReactElement => {
  const [maze, setMaze] = useState<GetMazeDataResponse>(props.mazeData)
  const [visited, setVisited] = useState<number[]>(props.mazeData.pony) // Will be useful for unimplemented self solve algorithm

  const { mazeId } = useParams()
  const imgUrl = new URLSearchParams(location.search).get('imgUrl')

  const navigate = useNavigate()

  const handleMovePony = useCallback(
    async (direction: Direction) => {
      if (!mazeId) return
      await apiCalls.movePony(mazeId, direction)
      const maze = await apiCalls.getMazeData(mazeId)

      const gs = maze['game-state']

      const url = (state: GameState['state']) =>
        `/maze/${mazeId}/${state}${gs['hidden-url']}?message=${gs['state-result']}`

      if (gs.state === 'won' || gs.state === 'over') {
        return navigate(url(gs.state))
      }

      setVisited([...visited, maze.pony[0]])
      setMaze(maze)
    },
    [visited]
  )

  const { pony, size } = maze
  const [width] = size
  const lastRow: number = maze.data.length - width
  const lastColumnItem = (i: number): boolean => !((i + 1) % width)

  return (
    <>
      <MazeGrid>
        {maze.data.map((item, index) => {
          const [ponyIndex] = pony

          const pn = PonyNavigator(
            { maze, ponyIndex, currentIndex: index },
            handleMovePony
          )

          const isPonyIndex = index === ponyIndex
          const isMonsterIndex = index === maze.domokun[0]
          const isEndpointIndex = index === maze['end-point'][0]
          const hasVisited = visited.includes(index)

          const arrowDirection = pn.mazeArrowDirection(index)
          const hasArrow = !!arrowDirection
          const blockCssClasses: string[] = item.filter((i) => i.length) // classes for north and west borders
          const isEndpointNextBlock = hasArrow && index === maze['end-point'][0]

          hasArrow && blockCssClasses.push('is-arrow') // If arrow exists, allow hover class
          index >= lastRow && blockCssClasses.push('last-row') // For adding the bottom border
          lastColumnItem(index) && blockCssClasses.push('last-column')
          isPonyIndex && blockCssClasses.push('isPonyIndex') // If arrow exists, allow hover class
          hasVisited && blockCssClasses.push('has-visited') // If we already passed over this block before

          const contentProps = () => {
            const obj: Record<string, any> = {}

            let label

            if (isPonyIndex) {
              label = 'Pony'
            } else if (isEndpointIndex) {
              label = 'Endpoint'
            } else if (isMonsterIndex) {
              label = 'Monster'
            }

            obj['aria-label'] = label

            if (isPonyIndex) {
              obj['aria-current'] = true
              obj['data-bgimg'] = imgUrl
            }

            return obj
          }

          return (
            <MazeSquare
              key={index}
              className={blockCssClasses.join(' ')}
              onClick={async () => pn.moveToThisIndex(index)}
              aria-label={arrowDirection && `go ${arrowDirection}`}
              role={arrowDirection && 'button'}
            >
              <Content {...contentProps()}>
                {hasArrow ? (
                  isEndpointNextBlock ? (
                    <Flag className={'flag'} />
                  ) : (
                    <MazeArrow direction={arrowDirection} />
                  )
                ) : (
                  !isPonyIndex && (
                    <>
                      {index === maze.domokun[0] && (
                        <Gremlin className={'monster icon'} />
                      )}
                      {index === maze['end-point'][0] && (
                        <Flag className={'flag icon'} />
                      )}
                    </>
                  )
                )}
              </Content>
            </MazeSquare>
          )
        })}
      </MazeGrid>
    </>
  )
}
