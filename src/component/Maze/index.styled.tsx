import styled from 'styled-components'
import { ColorPalette } from '../../enum'

export const MazeGrid = styled.div`
  display: flex;
  flex-wrap: wrap;
  max-width: 500px;
  margin-left: auto;
  margin-right: auto;
`

export const MazeSquare = styled.div`
  position: relative;
  width: calc(100% / 15);
  box-sizing: border-box;
  margin-top: -2px;

  // Top wall
  &.north {
    border-top: 2px solid ${ColorPalette.BLACK};
  }

  // Left wall
  &.west {
    border-left: 2px solid ${ColorPalette.BLACK};
  }

  // Bottom border of maze
  &.last-row {
    border-bottom: 2px solid ${ColorPalette.BLACK};
  }

  // Right border or maze
  &.last-column {
    border-right: 2px solid ${ColorPalette.BLACK};
  }

  &.is-arrow {
    cursor: pointer;
    backface-visibility: hidden;
    transition: all 0.4s;

    .arrow {
      stroke: ${ColorPalette.PINK};
      transition: all 0.4s;
    }

    &:hover {
      background: ${ColorPalette.PINK};

      .arrow {
        stroke: #ffffff;
      }
    }
  }

  &.has-visited {
    background-color: #fff0ce;
  }

  :before {
    content: '';
    display: block;
    padding-top: 100%;
  }
`

export const Content = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  display: flex; /* added for centered text */
  justify-content: center; /* added for centered text */
  align-items: center; /* added for centered text */
  background-position: center;
  background-size: cover;

  &[aria-current='true'] {
    border: 2px solid ${ColorPalette.PINK};
    width: calc(100% - 4px);
    height: calc(100% - 4px);
    z-index: 1;
  }

  .flag {
    stroke: ${ColorPalette.TEAL};
  }

  .monster {
    stroke: #000000;
  }

  ${(props: any) => {
    return `background-image: url(${props['data-bgimg']})`
  }}
`
