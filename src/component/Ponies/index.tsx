import { ButtonReset } from '../../App.styled'
import React, { ReactElement } from 'react'
import { Pony } from '../../types'
import { PoniesWrapper } from './index.styled'

type PropTypes = {
  ponies: Pony[]
  setPony: React.Dispatch<React.SetStateAction<Pony | null>>
}

const ValidPonies: string[] = [
  'Princess Twilight',
  'Applejack',
  'Fluttershy',
  'Rarity',
  'Pinkie Pie',
  'Rainbow Dash',
  'Spike',
  'Apple Bloom',
  'Sweetie Belle',
  'Princess Celestia',
  'Princess Luna',
  'Big McIntosh'
]

export const Ponies = ({ ponies, setPony }: PropTypes): ReactElement => (
  <PoniesWrapper>
    {ponies
      .filter((p) => ValidPonies.includes(p.name))
      .map((p) => (
        <ButtonReset className={'pony'} key={p.id} onClick={() => setPony(p)}>
          <figure>
            <figcaption>{p.name}</figcaption>
            <img src={p.image![0]} alt={p.name} title={p.name} />
          </figure>
        </ButtonReset>
      ))}
  </PoniesWrapper>
)
