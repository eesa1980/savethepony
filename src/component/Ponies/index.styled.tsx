import styled from 'styled-components'
import { ColorPalette } from '../../enum'

export const PoniesWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;

  .pony {
    align-items: center;
    justify-content: center;
    flex-grow: 0;
    display: block;
    width: 100%;
    max-width: 50%;
    flex-basis: 50%;
    overflow: hidden;

    @media screen and (min-width: 768px) {
      flex-basis: 25%;
    }

    figure {
      padding: 0;
      margin: 0;
      height: 100%;
      position: relative;
      background: ${ColorPalette.PINK};
    }

    figcaption {
      position: absolute;
      top: 0;
      z-index: 1;
      display: flex;
      align-items: center;
      justify-content: center;
      font-size: 20px;
      font-weight: bold;
      color: #ffffff;
      opacity: 0;
      transition: all 0.4s;
      padding: 20px;
      height: calc(100% - 40px);
      width: calc(100% - 40px);
      text-align: center;
    }

    img {
      object-position: center;
      object-fit: cover;
      width: 110%;
      height: 100%;
      opacity: 1;
      position: relative;
      display: block;
      transition: transform 0.25s, visibility 0.25s ease-in, opacity 0.25s;
    }

    &:hover {
      img {
        transform: scale(1.5);
        opacity: 0.1;
      }

      figcaption {
        opacity: 1;
      }
    }
  }
`
