import React, { ReactElement, useEffect, useState } from 'react'
import { ApiCalls } from '../../api/'
import { useNavigate } from 'react-router-dom'
import { Pony } from '../../types'

type PropTypes = {
  pony: Pony | null
}
const NOT_FOUND = 'NOT_FOUND'

const apiCalls = ApiCalls()

export const CreateMaze = (props: PropTypes): ReactElement => {
  const [maze_id, set] = useState<string | null>(null)
  const navigate = useNavigate()

  useEffect(() => {
    if (!props.pony) return

    apiCalls
      .createMaze({ name: props.pony.name })
      .then(({ maze_id }) =>
        navigate(`/maze/${maze_id}?imgUrl=${props.pony?.image?.[0]}`)
      )
      .catch((err) => {
        set(NOT_FOUND)
        console.error(err.response)
      })
  }, [props.pony])

  if (maze_id === NOT_FOUND) return <h2>Pony not found</h2>

  return <></>
}
