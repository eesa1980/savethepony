import React, { ReactElement, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { GetMazeDataResponse } from '../../types'

type PropTypes = {
  mazeData: GetMazeDataResponse
  children: () => ReactElement
}

export const GameStateChecker = ({ mazeData, children }: PropTypes) => {
  const navigate = useNavigate()

  useEffect(() => {
    const gs = mazeData['game-state']
    if (gs.state === 'won' || gs.state === 'over') {
      return navigate('/')
    }
  }, [])

  return children()
}
