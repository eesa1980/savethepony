import React, {
  JSXElementConstructor,
  ReactElement,
  useEffect,
  useState
} from 'react'
import { ApiCalls } from '../../api/'
import { Pony, PonyResponse } from '../../types'

type PropTypes = {
  id?: string
  children: (props?: PonyResponse['data']) => ReactElement
  showSpinner?: boolean
}

const apiCalls = ApiCalls()

export const FetchPonies = (props: PropTypes): ReactElement => {
  const [ponies, set] = useState<PonyResponse['data'] | null>(null)

  useEffect(() => {
    apiCalls
      .getPonies(props.id)
      .then((ponies) => set(ponies.data))
      .catch(console.error)
  }, [])

  return ponies?.length ? (
    props.children(ponies)
  ) : (
    <>{props.showSpinner && 'Fetching Ponies...'}</>
  )
}
