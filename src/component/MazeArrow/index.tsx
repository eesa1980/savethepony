import React, { ReactElement } from 'react'
import { Direction } from '../../enum'
import { LinkDown, LinkNext, LinkPrevious, LinkUp } from 'grommet-icons'

export const MazeArrow = ({
  direction
}: {
  direction: Direction
}): ReactElement => {
  switch (direction) {
    case Direction.NORTH:
      return <LinkUp className={'arrow icon'} />

    case Direction.SOUTH:
      return <LinkDown className={'arrow icon'} />

    case Direction.EAST:
      return <LinkNext className={'arrow icon'} />

    case Direction.WEST:
      return <LinkPrevious className={'arrow icon'} />
  }

  return <></>
}
