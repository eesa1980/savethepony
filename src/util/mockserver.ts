import {
  DefaultRequestBody,
  MockedRequest,
  RequestHandler,
  rest,
  RestRequest
} from 'msw'
import { BASE_URL, Endpoint, PONY_URL } from '../api/'
import { setupServer } from 'msw/node'
import {
  dummyMazeData,
  mockPonyResponse,
  mockSinglePonyResponse
} from './dummydata'

type RequestHandlers = RequestHandler<
  Record<string, any>,
  MockedRequest<DefaultRequestBody>,
  any,
  MockedRequest<DefaultRequestBody>
>

const mockserver = (...requestHandler: RequestHandlers[]) =>
  setupServer(
    ...requestHandler,
    rest.get(PONY_URL + Endpoint.PONIES_ALL, (req, res, ctx) => {
      return res(ctx.json(mockPonyResponse))
    }),
    rest.get(PONY_URL + '/1', (req, res, ctx) => {
      return res(ctx.json(mockSinglePonyResponse))
    }),
    rest.post(BASE_URL + Endpoint.MAZE, (req: RestRequest<any>, res, ctx) => {
      if (req.body['maze-player-name'] !== 'valid-pony') {
        return res(ctx.text('Only ponies can play'))
      }

      return res(
        ctx.json({
          maze_id: '0000-0000-0000-0000'
        })
      )
    }),
    rest.post(
      BASE_URL + Endpoint.MAZE + '/0000-0000-0000-0000',
      (req: RestRequest<any>, res, ctx) => {
        return res(
          ctx.json({ state: 'active', 'state-result': 'Move accepted' })
        )
      }
    ),
    rest.get(
      BASE_URL + Endpoint.MAZE + '/0000-0000-0000-0000',
      (req: RestRequest<any>, res, ctx) => {
        return res(ctx.json(dummyMazeData))
      }
    )
  )

export default mockserver
