import { Direction } from '../enum'
import { GetMazeDataResponse } from '../types'

type PonyNavigatorArgs = {
  maze: GetMazeDataResponse
  ponyIndex: number
  currentIndex: number
}

export const PonyNavigator = (
  { maze, ponyIndex, currentIndex }: PonyNavigatorArgs,
  callback: (d: Direction) => Promise<void>
) => {
  const [width] = maze.size

  const moveDirectionIndexObj = {
    north: ponyIndex - width,
    south: ponyIndex + width,
    east: ponyIndex + 1,
    west: ponyIndex - 1
  }

  const blockData = {
    current: maze.data?.[ponyIndex],
    south: maze.data?.[ponyIndex + width],
    east: maze.data?.[ponyIndex + 1]
  }

  const canMoveInThisDirection = (direction: Direction): boolean => {
    switch (direction) {
      case Direction.NORTH:
        return !blockData.current?.includes(Direction.NORTH)
      case Direction.EAST:
        return !blockData.east?.includes(Direction.WEST)
      case Direction.SOUTH:
        return !blockData.south?.includes(Direction.NORTH)
      case Direction.WEST:
        return !blockData.current?.includes(Direction.WEST)
      default:
        return false
    }
  }

  const mazeArrowDirection = (index: number): Direction | undefined => {
    let direction

    switch (index) {
      case moveDirectionIndexObj.north:
        direction = canMoveInThisDirection(Direction.NORTH) && Direction.NORTH
        break
      case moveDirectionIndexObj.east:
        direction = canMoveInThisDirection(Direction.EAST) && Direction.EAST
        break
      case moveDirectionIndexObj.south:
        direction = canMoveInThisDirection(Direction.SOUTH) && Direction.SOUTH
        break
      case moveDirectionIndexObj.west:
        direction = canMoveInThisDirection(Direction.WEST) && Direction.WEST
        break
    }

    return direction || undefined
  }

  const initMovement = async (direction: Direction): Promise<void> => {
    if (canMoveInThisDirection(direction)) {
      await callback(direction)
    }
  }

  return {
    mazeArrowDirection,
    moveToThisIndex: async (index: number) => {
      switch (index) {
        case moveDirectionIndexObj.south:
          return await initMovement(Direction.SOUTH)

        case moveDirectionIndexObj.north:
          return await initMovement(Direction.NORTH)

        case moveDirectionIndexObj.east:
          return await initMovement(Direction.EAST)

        case moveDirectionIndexObj.west:
          return await initMovement(Direction.WEST)
      }
    }
  }
}
