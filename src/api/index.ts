import axios, { AxiosResponse } from 'axios'
import {
  CreateMazeResponse,
  CreateMazeArgs,
  GetMazeDataResponse,
  MovePonyResponse,
  PonyResponse
} from '../types'
import { Direction } from '../enum'

export const BASE_URL = 'https://ponychallenge.trustpilot.com'
export const PONY_URL = 'http://ponyweb.ml/v1/character'

export const Endpoint = {
  MAZE: '/pony-challenge/maze',
  PONIES_ALL: '/all'
} as const

export const ApiCalls = () => {
  const getPonies = async (id?: string): Promise<PonyResponse> => {
    const response: AxiosResponse<PonyResponse> = await axios({
      baseURL: PONY_URL,
      url: id || Endpoint.PONIES_ALL,
      method: 'GET'
    })

    return response.data
  }

  const createMaze = async ({
    name,
    width = 15,
    height = 25,
    difficulty = 0
  }: CreateMazeArgs): Promise<CreateMazeResponse> => {
    const response: AxiosResponse<CreateMazeResponse> = await axios({
      baseURL: BASE_URL,
      url: Endpoint.MAZE,
      method: 'POST',
      data: {
        'maze-width': width,
        'maze-height': height,
        'maze-player-name': name,
        difficulty: difficulty
      }
    })

    return response.data
  }

  const movePony = async (
    mazeId: string,
    direction: Direction
  ): Promise<MovePonyResponse> => {
    const response: AxiosResponse<MovePonyResponse> = await axios({
      baseURL: BASE_URL,
      url: `${Endpoint.MAZE}/${mazeId}`,
      method: 'POST',
      data: {
        direction
      }
    })

    return response.data
  }

  const getMazeData = async (mazeId: string): Promise<GetMazeDataResponse> => {
    const response: AxiosResponse<GetMazeDataResponse> = await axios({
      baseURL: BASE_URL,
      url: `${Endpoint.MAZE}/${mazeId}`,
      method: 'GET'
    })

    return response.data
  }

  return {
    getPonies,
    createMaze,
    movePony,
    getMazeData
  }
}
