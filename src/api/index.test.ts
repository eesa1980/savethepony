import { ApiCalls } from './index'
import mockserver from '../util/mockserver'
import { SetupServerApi } from 'msw/node'
import {
  dummyMazeData,
  mockPonyResponse,
  mockSinglePonyResponse
} from '../util/dummydata'
import { Direction } from '../enum'

// Tests could have been more complete but to save time, I've only done the minimal happy path tests

describe('ApiCalls.tsx', () => {
  let server: SetupServerApi = mockserver()
  const apiCalls = ApiCalls()

  beforeAll(() => {
    server.listen()
  })

  afterAll(() => {
    server.close()
  })

  describe('getPonies', () => {
    it('should call getPonies successfully', async () => {
      const result = apiCalls.getPonies()
      await expect(result).resolves.toMatchObject(mockPonyResponse)
    })
  })

  describe('getSinglePony', () => {
    it('should call getPonies successfully', async () => {
      const result = apiCalls.getPonies('1')
      await expect(result).resolves.toMatchObject(mockSinglePonyResponse)
    })
  })

  describe('createMaze', () => {
    it('should call createMaze successfully', async () => {
      const result = apiCalls.createMaze({ name: 'valid-pony' })
      await expect(result).resolves.toMatchObject({
        maze_id: '0000-0000-0000-0000'
      })
    })
  })

  describe('movePony', () => {
    it('should PonyNavigator successfully', async () => {
      const result = apiCalls.movePony('0000-0000-0000-0000', Direction.WEST)

      await expect(result).resolves.toMatchObject({
        state: 'active',
        'state-result': 'Move accepted'
      })
    })
  })

  describe('getMazeData', () => {
    it('should getMazeData successfully', async () => {
      const result = apiCalls.getMazeData('0000-0000-0000-0000')
      await expect(result).resolves.toMatchObject(dummyMazeData)
    })
  })
})
