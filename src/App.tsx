import React, { ReactElement } from 'react'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import { Main } from './App.styled'
import { Maze } from './pages/Maze'
import { Result } from './pages/Result'
import { SelectPony } from './pages/SelectPony'

const App = (): ReactElement => {
  return (
    <Main>
      <Router>
        <Routes>
          <Route path="/maze/:mazeId" element={<Maze />} />
          <Route path="/maze/:mazeId/won/:hiddenUrl" element={<Result />} />
          <Route path="/maze/:mazeId/over/:hiddenUrl" element={<Result />} />
          <Route path="/" element={<SelectPony />} />
        </Routes>
      </Router>
    </Main>
  )
}

export default App
