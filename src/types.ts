import { StateResult } from './enum'

export type PonyResponse = {
  status: number
  data?: Array<Pony> | null | undefined
}

export type Pony = {
  id: number
  name: string
  alias?: string | null
  url: string
  sex: string
  residence?: string | null
  occupation?: string | null
  kind?: string[] | null
  image?: string[] | null
}

export type CreateMazeResponse = {
  maze_id: string
}

type Wall = 'west' | 'north'

export type GetMazeDataResponse = {
  pony: number[]
  domokun: number[]
  'end-point': number[]
  size: number[]
  difficulty: number
  data: Wall[][]
  maze_id: string
  'game-state': GameState
}

export type GameState = {
  state: 'over' | 'won' | 'active'
  'state-result':
    | 'Move accepted'
    | "Can't walk in there"
    | 'You won. Game ended'
    | 'You lost. Killed by monster'
  'hidden-url'?: string
}

export type MovePonyResponse = {
  state: string
  'state-result': string
}

export type CreateMazeArgs = {
  name: string
  width?: number
  height?: number
  difficulty?: number
}
