import { render, screen } from '../../setupTests'
import { Result } from './index'
import React from 'react'
import { BrowserRouter } from 'react-router-dom'

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'), // use actual for all non-hook parts
  useParams: () => ({
    hiddenUrl: 'https://via.placeholder.com/728x90.png'
  })
}))

describe('Result.tsx', () => {
  test('Renders the Result page with a title and a hero image once game is over', () => {
    jest.spyOn(URLSearchParams.prototype, 'get').mockReturnValue('You Won')

    render(
      // Need to wrap in BrowserRouter as a Link is used on page
      <BrowserRouter>
        <Result />
      </BrowserRouter>
    )

    expect(screen.getByText('You Won')).toBeInTheDocument()
    expect(screen.getByRole('img')).toBeInTheDocument()
    expect(screen.getByText('Play again')).toBeInTheDocument()
  })
})

export {}
