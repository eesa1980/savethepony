import React, { ReactElement } from 'react'
import { BASE_URL } from '../../api'
import { useParams } from 'react-router-dom'
import { ButtonLink, Container } from '../../App.styled'

export const Result = (): ReactElement => {
  const { hiddenUrl } = useParams()
  const message: string =
    new URLSearchParams(location.search).get('message') || ''

  return (
    <Container>
      <h1>{message}</h1>
      <img
        src={BASE_URL + '/' + hiddenUrl}
        width={'100%'}
        height={'auto'}
        alt={message}
        title={message}
      />
      <br />
      <br />
      <ButtonLink to={'/'}>Play again </ButtonLink>
    </Container>
  )
}
