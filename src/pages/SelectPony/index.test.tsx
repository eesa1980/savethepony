import { render, screen } from '../../setupTests'
import { SelectPony } from './index'
import React from 'react'
import { SetupServerApi } from 'msw/node'
import mockserver from '../../util/mockserver'
import { BrowserRouter } from 'react-router-dom'

describe('SelectPony.tsx', () => {
  let server: SetupServerApi = mockserver()

  beforeAll(() => {
    server.listen()
  })

  afterAll(() => {
    server.close()
  })

  test('Renders the SelectPony page with two ponies to select from', async () => {
    render(
      <BrowserRouter>
        <SelectPony />
      </BrowserRouter>
    )

    expect(screen.getByText('Pony Challenge')).toBeInTheDocument()
    expect(screen.getByText('Select a pony')).toBeInTheDocument()

    const buttons = await screen.findAllByRole('button')
    expect(buttons).toHaveLength(2)

    expect(screen.getByText('Applejack')).toBeInTheDocument()
    expect(screen.getByText('Fluttershy')).toBeInTheDocument()
  })
})

export {}
