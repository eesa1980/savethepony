import React, { useState } from 'react'
import { Pony } from '../../types'
import { FetchPonies } from '../../component/FetchPonies'
import { CreateMaze } from '../../component/CreateMaze'
import { Container } from '../../App.styled'
import { Ponies } from '../../component/Ponies'

export const SelectPony = () => {
  const [pony, setPony] = useState<Pony | null>(null)

  return (
    <Container>
      <h1>Pony Challenge</h1>
      <h2>Select a pony</h2>
      <FetchPonies>
        {(ponies) => (
          <>
            <Ponies ponies={ponies || []} setPony={setPony} />
            <CreateMaze pony={pony} />
          </>
        )}
      </FetchPonies>
    </Container>
  )
}
