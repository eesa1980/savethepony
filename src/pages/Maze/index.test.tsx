import { render, screen } from '../../setupTests'
import { Maze } from './index'
import React from 'react'
import { SetupServerApi } from 'msw/node'
import mockserver from '../../util/mockserver'
import { Endpoint } from '../../api'

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'), // use actual for all non-hook parts
  useParams: () => ({
    mazeId: '0000-0000-0000-0000'
  }),
  useNavigate: () => jest.fn()
}))

describe('Maze.tsx', () => {
  let server: SetupServerApi = mockserver()

  beforeAll(() => {
    server.listen()
  })

  afterAll(() => {
    server.close()
  })

  test('Renders the Maze page which includes a pony, monster and a home icon', async () => {
    render(<Maze />)

    await expect(
      screen.findByLabelText('Endpoint')
    ).resolves.toBeInTheDocument()
    await expect(screen.findByLabelText('Monster')).resolves.toBeInTheDocument()
    await expect(screen.findByLabelText('Pony')).resolves.toBeInTheDocument()
    await expect(
      screen.findByLabelText('go north' || 'go south' || 'go east' || 'go west')
    ).resolves.toBeInTheDocument() // The nav arrows
  })
})

export {}
