import React from 'react'
import { FetchMaze } from '../../component/FetchMaze'
import { Maze as MazeComponent } from '../../component/Maze'
import { useParams } from 'react-router-dom'
import { GameStateChecker } from '../../component/GameStateChecker'
import { Container } from '../../App.styled'

export const Maze = () => {
  const { mazeId } = useParams()

  return (
    <Container>
      <FetchMaze mazeId={mazeId}>
        {(maze) => (
          <GameStateChecker mazeData={maze}>
            {() => <MazeComponent mazeData={maze} />}
          </GameStateChecker>
        )}
      </FetchMaze>
    </Container>
  )
}
