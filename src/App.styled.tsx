import styled from 'styled-components'
import { ColorPalette } from './enum'
import { Link } from 'react-router-dom'

export const Main = styled.main`
  color: ${ColorPalette.BLACK};
  padding: 20px;
  margin: auto;
  font-size: 15px;

  * {
    font-family: 'Roboto', sans-serif;
  }

  h1 {
    text-align: center;
    font-size: 25px;
    margin-bottom: 30px;
    font-weight: bold;
  }

  h2 {
    text-align: center;
    font-size: 20px;
    margin-bottom: 30px;
  }

  .icon {
    max-width: 100%;
  }
`

export const Container = styled.div`
  max-width: 800px;
  margin: auto;
`

export const ButtonLink = styled(Link)`
  padding: 10px 20px;
  border-radius: 5px;
  text-decoration: none;
  color: ${ColorPalette.BLACK};
  border: 1px solid ${ColorPalette.BLACK};
  display: inline-block;
  transition: all 0.2s;

  &:hover {
    background: ${ColorPalette.PINK};
    border-color: ${ColorPalette.PINK};
    color: #fff;
  }
`

export const ButtonReset = styled.button`
  background: none repeat scroll 0 0 transparent;
  border: medium none;
  border-spacing: 0;
  color: ${ColorPalette.BLACK};
  font-size: 16px;
  font-weight: normal;
  line-height: 1.42rem;
  list-style: none outside none;
  margin: 0;
  padding: 0;
  text-align: left;
  text-decoration: none;
  text-indent: 0;

  &::-moz-focus-inner {
    border: 0;
    padding: 0;
  }
`
