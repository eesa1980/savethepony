# Save The Pony Coding Challenge
Thanks for taking the time to review my project. Please find below details on how to get it up and running:



### Download the project
First clone the project
``` 
$ git clone https://gitlab.com/eesa1980/savethepony.git savethepony
$ cd savethepony
```
### Running the project
Then start the project
```
$ yarn install && yarn start
```
### Running the tests
```
$ yarn test
```

### How to play
1. On the homepage, select your favourite pony
2. Click on the pony to load a random maze
3. Once the maze has loaded, use the arrows by clicking on them to navigate around the maze
4. Avoid the moving monster, otherwise you will lose and need to start again 
5. Reach the endpoint flag to win the game!
